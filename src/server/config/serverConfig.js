module.exports = {
	version: '0.3.0',
	port: 4000,
	startupMessage: 'Server: ready',
	defaultZone: 'fjolarok'
};
